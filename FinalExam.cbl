       IDENTIFICATION DIVISION. 
       PROGRAM-ID. TRADER.
       AUTHOR. SAKSSIT.
     
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT TRADER-FILE ASSIGN TO "trader3.dat"
           ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION. 
       FD  TRADER-FILE.
       01  INPUT-BUFFER.
           88 END-OF-TRADER-FILE        VALUE HIGH-VALUES.
       05  COL-A          PIC X(2).
           05 COL-COUNT   PIC 9(3).
       WORKING-STORAGE SECTION. 
       01  TOTAL                PIC 9(4)   VALUE ZEROS.
       01  COL-A-TOTAL          PIC 9(4).
       01  COL-A-PROCESSING     PIC X(2).
       01  RPT-HEADER.
           05 FILLER            PIC X(9)   VALUE "PROVINCE".
           05 FILLER            PIC X(4)   VALUE SPACES.
           05 FILLER            PIC X(9)  VALUE "P INCOME".
           05 FILLER            PIC X(4)   VALUE SPACES.
           05 FILLER            PIC X(9)   VALUE "MEMBER".
           05 FILLER            PIC X(15)   VALUE "MEMBER INCOME".
       01  RPT-ROW.
           05 RPT_COL-A         PIC BBX(2).
           05 FILLER            PIC X(5)   VALUE SPACES.
           05 RPT-COL-A-TATOL   PIC 9(9).
       01  RPT_FOOTER.
           05 FILLER            PIC X(15)   VALUE "  SUM INCOME:".
           05 RPT-TOTAL         PIC 9(9).
       PROCEDURE DIVISION .
       BEGIN.
           OPEN INPUT   TRADER-FILE
           DISPLAY  RPT-HEADER 
           PERFORM  READ-LINE
           PERFORM  PROCEDURE-COL-A UNTIL END-OF-TRADER-FILE
           MOVE TOTAL TO RPT-TOTAL 
           DISPLAY  RPT_FOOTER 
           CLOSE  TRADER-FILE
           GOBACK 
           .
       PROCEDURE-COL-A.
           MOVE COL-A TO COL-A-PROCESSING
           MOVE ZEROS TO COL-A-TOTAL
           PERFORM  PROCEDURE-LINE UNTIL COL-A NOT = COL-A-PROCESSING 
           MOVE COL-A-PROCESSING TO RPT_COL-A
           MOVE COL-A-TOTAL TO RPT-COL-A-TATOL
           DISPLAY  "  "RPT-ROW 
           .
       PROCEDURE-LINE.
           ADD COL-COUNT TO TOTAL, COL-A-TOTAL 
           PERFORM READ-LINE
           .
       READ-LINE.
           READ  TRADER-FILE
           AT END   
              SET END-OF-TRADER-FILE TO TRUE
           END-READ
           .




        
